import requests

class EncodeTFs:
	def __init__(self, genome):
		self.__url__ = "https://www.encodeproject.org/search/?type=AntibodyLot&status=released&targets.investigated_as=transcription+factor&targets.organism.scientific_name=Homo+sapiens&targets.organism.scientific_name=Mus+musculus&format=json&frame=embedded&&limit=all"

		specie = ""
		if genome.lower() in ["hg19", "hg38"]:
			specie = "Homo sapiens"

		self.__specie__ = specie
		self.__tfs__ = {}

	def init(self):
		payload = {}
		payload["targets.organism.scientific_name"] = self.__specie__
		response = requests.get(self.__url__, params=payload)
		experiment = response.json()
		tfs = experiment["@graph"]

		self.__tfs__ = {}
		for tf in tfs:
			metadata = {}
			metadata["encode_id"] = tf["@id"]

			investigated_as_set = set()
			for characterization in tf.get("characterizations", []):
				target = characterization.get("target", {})
				for investigated_as in target.get("investigated_as", []):
					investigated_as_set.add(investigated_as)

			investigated_as_list = list(investigated_as_set)
			for k in xrange(len(investigated_as_list)):
				metadata["investigated_as_"+str(k)] = investigated_as_list[k]

			for k in xrange(len(tf.get("dbxrefs", []))):
				metadata["dbxref_"+str(k)] = tf["dbxrefs"][k]

			gene_names_as_set = set()
			for target in tf.get("targets", []):
				for gene in target.get("genes", []):
					name = gene.get("name", "")
					if name:
						gene_names_as_set.add(name)

			genes_as_list = list(gene_names_as_set)
			for k in xrange(len(genes_as_list)):
				metadata["gene_name_"+str(k)] = genes_as_list[k]

			labels_as_set = set()
			for target in tf.get("targets", []):
				label = target.get("label", "")
				if label:
					labels_as_set.add(label)

			labels_as_list = list(labels_as_set)
			for k in xrange(len(labels_as_list)):
				metadata["label_"+str(k)] = labels_as_list[k]

			for label in labels_as_list:
				self.__tfs__[label.lower()] = metadata

		print(self.__tfs__.get("CTCF", "shit"))
		print(self.__tfs__.get("ctcf", "shit"))




	def __getitem__(self, tf_name):
		if not self.__tfs__:
			self.init()

		tf = self.__tfs__.get(tf_name, None)
		if not tf:
			return None

		return tf
