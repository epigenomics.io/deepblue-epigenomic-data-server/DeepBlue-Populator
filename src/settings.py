import platform
import os
import logging


"""
Config Variables
"""

if platform.system() == "Darwin":
    OS = "macosx"
else:
    OS = "linux"

EPIDB_INIT_USER = ("DeepBlue Epigenomic Data Server", "data.server@epigenomics.io", "epigenomics.io")
EPIDB_POPULATOR_USER = ("Populator", "data.populator@epigenomics.io", "epigenomics.io")
EPIDB_AUTHKEY_FILE = ".populator.epidb"

LOG_LEVEL = logging.DEBUG

#MDB_HOST = "localhost"
MDB_HOST ="mongodb://localhost:27017,localhost:27018,localhost:27019/?replicaSet=rs"
MDB_PORT = 27027

DEEPBLUE_HOST = "localhost"
DEEPBLUE_PORT = 31415

DOWNLOAD_PATH = os.getenv('DOWNLOAD_PATH', '/mnt/download')
DATA_DIR = os.getenv('DATA_PATH', '/mnt/data')

def in_download(path):
    return os.path.join(DOWNLOAD_PATH, path)

def in_data(path):
    return os.path.join(DATA_DIR, path)

"""
Threads
"""
max_threads = 4
