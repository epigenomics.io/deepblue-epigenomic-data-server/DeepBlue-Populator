import requests
import sys
import tempfile

import settings

settings.DATA_DIR

link =  "https://gitlab.com/epigenomics.io/deepblue-epigenomic-data-server/deepblue-populator-data/-/archive/main/deepblue-populator-data-main.zip"
tmp_file = tempfile.mkstemp(
                prefix = settings.in_data("deepblue-populator-data-main"),
                suffix= "_tmp.zip"
            )

file_name = tmp_file[1]

with open(file_name, "wb") as f:
    print("Downloading %s from %s" % (file_name, link))
    response = requests.get(link, stream=True)
    total_length = response.headers.get('content-length')

    if total_length is None: # no content length header
        f.write(response.content)
    else:
        dl = 0
        total_length = int(total_length)
        for data in response.iter_content(chunk_size=4096):
            print("s")
            dl += len(data)
            f.write(data)
            done = int(50 * dl / total_length)
            sys.stdout.write("\r[%s%s]" % ('=' * done, ' ' * (50-done)) )
            sys.stdout.flush()

