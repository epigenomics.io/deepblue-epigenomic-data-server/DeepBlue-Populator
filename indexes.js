db.biosource_embracing.ensureIndex({"norm_biosource_name": "hashed" })

db.biosource_synonyms_names.ensureIndex({"norm_synonym": "hashed"})

db.biosources.ensureIndex({"extra_metadata.ontology_id": "hashed"})
db.biosources.ensureIndex({"norm_name": "hashed"})

db.counters.ensureIndex({"_id": "hashed"})

db.jobs.ensureIndex({"state": "hashed"})

db.projects.ensureIndex({"_id": "hashed"})

db.projects.ensureIndex({"public": "hashed"})

db.text_search.ensureIndex({"type": "hashed"})
db.text_search.ensureIndex({"epidb_id": "hashed"})
db.text_search.ensureIndex({"norm_biosource_name": "hashed"})
db.text_search.ensureIndex({"sample_info_norm_biosource_name": "hashed"})

db.users.ensureIndex({"key": "hashed"})

db.gene_ontology.ensureIndex({"go_id": "hashed"})
db.gene_ontology.ensureIndex({"go_label": "hashed"})

db.genes.ensureIndex({"ATTRIBUTES.gene_id":"hashed"})
db.genes.ensureIndex({"ATTRIBUTES.gene_id": 1 })

