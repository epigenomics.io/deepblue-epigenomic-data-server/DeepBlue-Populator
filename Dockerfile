# Dockerfile for DeepBlue Populator
# Felipe Albrecht - 25.01.2022

FROM ubuntu:21.04
LABEL Name=DeepBlue-Populator Version=1.5.0

ENV DEBIAN_FRONTEND=noninteractive


RUN apt-get update
RUN apt-get install --yes \
      git \
      wget \
      unzip \
      python2.7

# Checkout DeepBlue Server code and libs
WORKDIR /
RUN git clone https://gitlab.com/epigenomics.io/deepblue-epigenomic-data-server/DeepBlue-Populator.git

## Build LuaJIT
WORKDIR  /DeepBlue-Populator/

ENV LD_LIBRARY_PATH=/DeepBlue/
#ENTRYPOINT ["./server"]
CMD ["python2", "main.py", "--help"]
